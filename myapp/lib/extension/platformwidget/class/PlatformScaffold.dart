import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp/extension/platformwidget/platform_widget.dart';

class PlatformScaffold extends PlatformWidget<Scaffold, Scaffold> {
  final String title;
  final String iospreviousPageTitle;
  final Widget child;

  PlatformScaffold({this.title, this.child, this.iospreviousPageTitle});

  @override
  Scaffold createAndroidWidget(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(title),
      ),
      body: child,
    );
  }

  @override
  Scaffold createIosWidget(BuildContext context) {
    return new Scaffold(
      appBar: new CupertinoNavigationBar(
        middle: new Text(title),
        previousPageTitle: iospreviousPageTitle,
      ),
      body: child,
    );
  }
}