import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp/extension/platformwidget/platform_widget.dart';

class PlatformSliverNavigationBar
    extends PlatformWidget<CupertinoSliverNavigationBar, SliverAppBar> {
  final String title;
  final Widget androidaction;
  final Widget iostrailing;


  PlatformSliverNavigationBar({this.title, this.androidaction, this.iostrailing});

  @override
  SliverAppBar createAndroidWidget(BuildContext context) {
    return new SliverAppBar(
      actions: <Widget>[androidaction,],
      expandedHeight: 150.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
          title: Text(title,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              )),
          ),
    );
  }

  @override
  CupertinoSliverNavigationBar createIosWidget(BuildContext context) {
    return CupertinoSliverNavigationBar(
        largeTitle: Text(title), trailing: iostrailing);
  }
}
