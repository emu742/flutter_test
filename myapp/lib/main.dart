import 'package:flutter/material.dart';
import 'package:myapp/screens/RandomWords.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      home: RandomWords(),
      theme: new ThemeData(
        primaryColor: Colors.deepOrange,
        accentColor: Colors.deepOrangeAccent
      ),
    );
  }
}
