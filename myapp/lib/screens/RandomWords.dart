import 'package:english_words/english_words.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myapp/extension/platformwidget/class/PlatformCupertinoSliverNavigationBar.dart';
import 'package:myapp/extension/platformwidget/class/PlatformScaffold.dart';
import 'package:myapp/extension/platformwidget/class/Platformbutton.dart';

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => new RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _saved = new Set<WordPair>();
  final _biggerFont = const TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(slivers: <Widget>[
      PlatformSliverNavigationBar(
          title: 'Test',
          androidaction: new IconButton(icon: const Icon(Icons.list), onPressed: _pushSaved),
          iostrailing: PlatformButton(
            child: Text(
              'Filter',
              style: _biggerFont,
            ),
            onPressed: _pushSaved,
          )),
      SliverPadding(
          padding: const EdgeInsets.all(16.0), sliver: _buildSuggestions())
    ]));
  }

  Widget _buildSuggestions() {
    return SliverFixedExtentList(
      itemExtent: 30.0, // I'm forcing item heights
      delegate: SliverChildBuilderDelegate((BuildContext context, int i) {
        if (i.isOdd) return Divider();

        // The syntax "i ~/ 2" divides i by 2 and returns an integer result.
        // For example: 1, 2, 3, 4, 5 becomes 0, 1, 1, 2, 2.
        // This calculates the actual number of word pairings in the ListView,
        // minus the divider widgets.
        final index = i ~/ 2;
        // If you've reached the end of the available word pairings...
        if (index >= _suggestions.length) {
          // ...then generate 10 more and add them to the suggestions list.
          _suggestions.addAll(generateWordPairs().take(10));
        }
        return new Container(
          child: _buildRow(_suggestions[index]),
        );
      }, childCount: 20),
    );
  }

  Widget _buildRow(WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          pair.asPascalCase,
          style: _biggerFont,
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              if (alreadySaved) {
                _saved.remove(pair);
              } else {
                _saved.add(pair);
              }
            });
          },
          child: Icon(
            alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? Colors.red : null,
          ),
        ),
      ],
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _saved.map(
            (WordPair pair) {
              return new ListTile(
                  title: Text(
                pair.asPascalCase,
                style: _biggerFont,
              ));
            },
          );
          final List<Widget> divided = ListTile.divideTiles(
            context: context,
            tiles: tiles,
          ).toList();

          return new PlatformScaffold(
            // Add 6 lines from here...
            title: "Here we go!",
            iospreviousPageTitle: 'Test',
            child: ListView(
              children: divided,
            ),
          );
        },
      ),
    );
  }
}
